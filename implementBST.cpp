#include <iostream>
#include <climits>

using namespace std;

typedef struct Node {
	int data;
	struct Node* left;
	struct Node* right;
} Node;

Node* root = NULL;

Node* insertNode(Node* &root, int data) {
	if (root == NULL) {
		Node* newNode = new Node;
		newNode->data = data;
		newNode->left = newNode->right = NULL;
		root = newNode;
	} else if (data < root->data) {
		root->left = insertNode(root->left, data);
	} else {
		root->right = insertNode(root->right, data);
	}
	return root;
}

void displayBST(Node* root) {
	if (root != NULL) {
		cout << root->data << " ";
		displayBST(root->left);
		displayBST(root->right);
	}
}

bool search(Node* root, int data) {
	if (root == NULL) {
		return false;
	} else {
		if (root->data == data) {
			return true;
		} else if (data < root->data) {
			search(root->left, data);
		} else {
			search(root->right, data);
		}
	}
}

bool BSTcondition(Node* root, int min, int max) {
	if(root == NULL || root->data >= min && root->data <= max
		&& BSTcondition(root->left, min, root->data)
		&& BSTcondition(root->right, root->data, max)) {
			return true;
	}
	return false;
}

bool isBinarySearchTree(Node* root) {
	return BSTcondition(root, INT_MIN, INT_MAX);
}

Node* findMin(Node* root) {
	Node* current = root;
	while(current->left != NULL) {
		current = current->left;
	}
	return current;
}

Node* deleteNode(Node* root, int deleteValue) {
	if(deleteValue < root->data) {
		root->left = deleteNode(root->left, deleteValue);
	} else if (deleteValue > root->data) {
		root->right = deleteNode(root->right, deleteValue);
	} else {
		if(root->left == NULL && root->right == NULL) {
			delete root;
			root = NULL;
		} else if (root->left == NULL) {
			Node* temp = root;
			root = root->right;
			delete temp;
			temp = NULL;
		} else if (root->right == NULL) {
			Node* temp = root;
			root = root->left;
			delete temp;
			temp = NULL;
		} else {
			Node* minValue = findMin(root->right);
			root->data = minValue->data;
			root->right = deleteNode(root->right, minValue->data);
		}
	}
	return root;
}

int main() {
	insertNode(root, 30);
	insertNode(root, 20);
	insertNode(root, 40);
	insertNode(root, 10);
	insertNode(root, 15);
	insertNode(root, 35);
	insertNode(root, 34);
	insertNode(root, 36);
	
	displayBST(root);
	cout << "\n";
	
	if(search(root, 35)) {
		cout << "Found\n" << endl;
	} else {
		cout << "Not found\n" << endl;
	}
	
	root->left->data = 50;
	displayBST(root);
	cout << "\n";
	
	if(isBinarySearchTree(root)) {
		cout << "This is a binary search tree.\n" << endl;
	} else {
		cout << "This isn't a binary search tree.\n" << endl;
	}

	root->left->data = 20;
	displayBST(root);
	cout << "\n";

	deleteNode(root, 20);
	displayBST(root);
	cout << "\n";

	if(isBinarySearchTree(root)) {
		cout << "This is a binary search tree.\n" << endl;
	} else {
		cout << "This isn't a binary search tree.\n" << endl;
	}
	
	deleteNode(root, 35);
	displayBST(root);
	cout << "\n";

	if(isBinarySearchTree(root)) {
		cout << "This is a binary search tree.\n" << endl;
	} else {
		cout << "This isn't a binary search tree.\n" << endl;
	}
	return 0;
}
